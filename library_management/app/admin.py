from django.contrib import admin

from . import models


@admin.register(models.Book)
class BookAdmin(admin.ModelAdmin):
    list_display = ("id", "title")


@admin.register(models.BookCopy)
class BookCopyAdmin(admin.ModelAdmin):
    list_display = ("id", "year_published")


@admin.register(models.LibClient)
class LibClientAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "date_joined")


@admin.register(models.Loan)
class LoanAdmin(admin.ModelAdmin):
    list_display = ("id", "copy", "client", "start_date", "return_date")
