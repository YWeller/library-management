from rest_framework import status
from rest_framework.exceptions import APIException


class AlreadyRatedException(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = "User has already rated this book"
    default_code = "invalid"


class CurrentlyLentException(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = "This book is currently lent out"
    default_code = "invalid"
