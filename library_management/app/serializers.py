from collections import OrderedDict
from .exceptions import AlreadyRatedException, CurrentlyLentException
from .models import Book, BookCopy, Loan, LibClient, Rating
from rest_framework import serializers


class BookCopySerializer(serializers.ModelSerializer):
    rating = serializers.SerializerMethodField()
    title = serializers.CharField(read_only=True, source="book.title")

    def get_rating(self, obj):
        """ Gets rating for book object """
        return Book.rating(obj.book)

    class Meta:
        model = BookCopy
        fields = ("book", "title", "year_published", "rating")


class SlimBookCopySerializer(serializers.ModelSerializer):
    class Meta:
        model = BookCopy
        fields = ("id", "year_published")


class BookSerializer(serializers.ModelSerializer):
    """ Book serializer with included methods to get rating and copy count. """
    copies = SlimBookCopySerializer(many=True, read_only=True)
    year_published = serializers.CharField(write_only=True)
    rating = serializers.SerializerMethodField()
    copy_count = serializers.SerializerMethodField()

    def get_rating(self, obj):
        return Book.rating(obj)

    def get_copy_count(self, obj):
        return obj.copies.count()

    class Meta:
        model = Book
        fields = ("id", "title", "rating", "copy_count", "copies", "year_published")

    def create(self, validate_data):
        """ Create a book copy along with creating a book """
        year_published = validate_data.pop("year_published", None)
        book = Book.objects.create(**validate_data)
        if year_published:
            BookCopy.objects.create(book=book, year_published=year_published)
        return book


class LibClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = LibClient
        fields = ("id", "name")


class LoanSerializer(serializers.ModelSerializer):
    book_id = serializers.ReadOnlyField(source="book.id")
    client_name = serializers.ReadOnlyField(source="client.user.username")
    title = serializers.ReadOnlyField(source="book.title")
    year_published = serializers.ReadOnlyField(source="copy.year_published")
    client = serializers.PrimaryKeyRelatedField(
        required=True, queryset=LibClient.objects.all()
    )
    copy = serializers.PrimaryKeyRelatedField(
        required=True, queryset=BookCopy.objects.all()
    )

    class Meta:
        model = Loan
        fields = (
            "id",
            "book_id",
            "copy",
            "title",
            "year_published",
            "client_name",
            "client",
            "start_date",
            "return_date",
        )

    def to_internal_value(self, data: OrderedDict):
        """ Delete return_date if included in instance creation """
        data = super().to_internal_value(data)
        if not self.instance:
            data.pop("return_date", None)
        return data

    def validate(self, attrs: OrderedDict):
        """ Check if book is available """
        copy = attrs.get("copy")
        if not copy.check_available():
            raise CurrentlyLentException
        else:
            return attrs

    def create(self, validated_data):
        """ Include FK field book """
        book = validated_data.get("copy").book
        return Loan.objects.create(book=book, **validated_data)


class RatingSerializer(serializers.ModelSerializer):
    client = serializers.PrimaryKeyRelatedField(
        required=True, queryset=LibClient.objects.all()
    )

    class Meta:
        model = Rating
        fields = ("id", "book", "client", "rating")

    def validate(self, attrs: OrderedDict):
        book = attrs.get("book")
        client = attrs.get("client")
        if not Rating.check_can_rate(book, client):
            raise AlreadyRatedException
        else:
            return attrs


class ReturnBookSerializer(serializers.Serializer):
    """ Serializer to validate book return """
    client = serializers.PrimaryKeyRelatedField(
        required=True, queryset=LibClient.objects.all()
    )
    copy = serializers.PrimaryKeyRelatedField(
        required=True, queryset=BookCopy.objects.all()
    )
