from datetime import datetime, timedelta
from json import JSONDecodeError
from django.http import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.mixins import CreateModelMixin, ListModelMixin
from rest_framework.viewsets import GenericViewSet
from .models import Book, BookCopy, Loan, LibClient
from rest_framework import viewsets, generics, status
from .serializers import (
    BookSerializer,
    BookCopySerializer,
    LibClientSerializer,
    LoanSerializer,
    ReturnBookSerializer,
)


# ViewSets include all methods needed for List, Create, Update, Delete
class BookViewSet(viewsets.ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer


class BookCopyViewSet(viewsets.ModelViewSet):
    queryset = BookCopy.objects.all()
    serializer_class = BookCopySerializer


class LibClientView(viewsets.ModelViewSet):
    queryset = LibClient.objects.all()
    serializer_class = LibClientSerializer


class LoanView(CreateModelMixin, ListModelMixin, GenericViewSet):
    queryset = Loan.objects.all()
    serializer_class = LoanSerializer


class ListOldLoansView(generics.ListAPIView):
    """ Returns a list of loans older then 45 days. """
    serializer_class = LoanSerializer

    def get_queryset(self):
        days = 45
        return Loan.objects.filter(
            start_date__lte=datetime.now() - timedelta(days=days)
        )


class CheckBookAvailabilityView(APIView):
    """ Checks for book availability. """
    def get(self, request, *args, **kwargs):
        copy = BookCopy.objects.filter(id=self.kwargs.get("id"))[0]
        if copy.check_available():
            return Response({"Available": True}, status=status.HTTP_200_OK)
        else:
            return Response({"Available": False}, status=status.HTTP_200_OK)


class ReturnBookView(generics.UpdateAPIView):
    """ Return a book, sets return date to current date """
    serializer_class = ReturnBookSerializer

    def update(self, request, *args, **kwargs):
        try:
            data = JSONParser().parse(request)
            serializer = ReturnBookSerializer(data=data)
            if serializer.is_valid(raise_exception=True):
                client = serializer.data.get("client")
                copy = serializer.data.get("copy")
                loan = Loan.objects.filter(copy=copy, client=client, return_date=None)[0]
                loan.return_date = datetime.now()
                loan.save()
                return Response({"Success": True}, status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except JSONDecodeError:
            return JsonResponse(
                {"result": "error", "message": "Json decoding error"}, status=400
            )

