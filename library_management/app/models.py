import uuid
from django.db import models
from django.db.models import Avg


class Book(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    title = models.CharField(max_length=255, unique=True)

    class Meta:
        ordering = ["id"]

    def __str__(self):
        return self.title

    @staticmethod
    def rating(book):
        """ return average rating from all related ratings """
        return list(book.ratings.aggregate(Avg("rating")).values())[0]


class BookCopy(models.Model):
    """ One book can have many copies """
    book = models.ForeignKey(Book, on_delete=models.CASCADE, related_name="copies")
    year_published = models.IntegerField()

    class Meta:
        ordering = ["year_published"]

    def __str__(self):
        return f"{self.book} - {self.year_published}"

    def check_available(self):
        lent = Loan.objects.filter(copy=self, return_date=None)
        if len(lent) > 0:
            return False
        else:
            return True

    def lend(self, client):
        if self.check_available():
            loan = Loan.objects.create(book=self.book, copy=self, client=client)
            return loan
        else:
            return None


class LibClient(models.Model):
    """ Model for Library Client """
    name = models.CharField(max_length=255)
    date_joined = models.DateField(auto_now_add=True)

    def __str__(self):
        return f"{self.name}, joined: {self.date_joined}"


class Loan(models.Model):
    """ Lend books, if returned date is null, it means book is still on loan """
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    copy = models.ForeignKey(
        BookCopy, on_delete=models.SET_NULL, null=True, related_name="loans"
    )
    client = models.ForeignKey(
        LibClient, on_delete=models.SET_NULL, null=True, related_name="loans"
    )
    start_date = models.DateField(auto_now_add=True)
    return_date = models.DateField(blank=True, null=True)

    def __str__(self):
        return f"{self.book} - {self.client}, {self.start_date}"


class Rating(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE, related_name="ratings")
    client = models.ForeignKey(LibClient, on_delete=models.SET_NULL, null=True)
    rating = models.SmallIntegerField()

    class Meta:
        unique_together = ('book', 'client')

    def __str__(self):
        return f"{self.book} - {self.client}, rating of: {self.rating}"

    @staticmethod
    def check_can_rate(book, client):
        """ Only one rating per client per book """
        ratings = Rating.objects.filter(book=book, client=client)
        if len(ratings) > 0:
            return False
        else:
            return True
