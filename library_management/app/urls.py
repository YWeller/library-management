from django.urls import path
from rest_framework import routers

from . import views

# Default router for ViewSet Urls and url patterns for single views
router = routers.DefaultRouter()
router.register(r"books", views.BookViewSet)
router.register(r"copies", views.BookCopyViewSet)
router.register(r"loans", views.LoanView)
router.register(r"libclients", views.LibClientView)

urlpatterns = router.urls

urlpatterns += [
    path("old-loans/", views.ListOldLoansView.as_view()),
    path("check-availability/<int:id>", views.CheckBookAvailabilityView.as_view()),
    path("return-book/", views.ReturnBookView.as_view()),
]
