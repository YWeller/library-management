# Library Management Project

#### This is a library management project to fulfill these specifications.

* Retrieve a list of books (title, year, and rating)
* Add a book to the library
* Update a book in the library
* Delete a book from the library
* Loan a book
* Check if the book is available (there can be more than one replica of the book)
* Return a book
* Check who has a specific book (not only the name but the book itself)
* Return a list with all the books that have been loan for more than 45 days
* Register a library client
* Get a library client
* Delete a library client
* Update a library client

### Database schema
For the database schema, I created a Book object and a BookCopy table, this allows for a book to have multiple copies.
It also helps to keep track of the status of each copy and who has borrowed it.
The LibClient table is a table for a Library Client with just name and date_joined.
Loan table which is related to the Book and BookCopy allows for data to persist even if the BookCopy was deleted,
and it's also related to LibClient. Return_date is to know if the book has been returned.
I kept ratings in a separate table to keep the logic separate and also allow for the data to be more dynamic.

### Project setup
I used django rest framework to create serializers and Rest Views, and Routers for URLs of the ViewSets.
Included is an SQLite Database. A username and password for a superuser are username: testuser, password: test_user_password


To run the project:
```
pip install -r requirements.txt
python manage.py runserver
```

The root URL for the API is: http://localhost:8000/api/v1/library/ 
this should go to the django rest framework user interface to allow for easy querying.
http://localhost:8000/admin/ takes a user to the admin site.

### Next steps 
* Complete the rating endpoints, and add any other relevant fields for a library management system.
* Add Unit tests to test all endpoints. 
* Dockerize the project, add dockerfile, docker-compose and a MySQL database. 
* Connect LibClient with one to one relationship to User table.
* Add permissions, read only for client user on some endpoints, read write for staff users.
* Use token auth where clients send an auth token with every request, (this can replicate how a library card would work.)

